#!/usr/bin/env python
import time
import read_cris_data_files_v2
import warnings
warnings.filterwarnings("ignore")


def main():
    start_time =time.time()

   
    pathToSdr = 'C:\Users\sbhatta\Desktop\cris_15_FSR_data'
      
    
    print " Select the options below"
    print " Enter the required number from the list below"

    print " 1 -->  Quality flag 1"
    print " 2 -->  Quality flag 2"
    print " 3 -->  Quality flag 3"
    print " 4 -->  Quality flag 4"
    
    print " 5 -->  DS Window size"
    print " 6 -->  ICT Window size"
    print " 7 -->  Es zpd amplitude"
    print " 8 -->  Es zpd fringe count"
    print " 9 -->  Sdr Fringe count"
    print " 10 --> Es RDR impulse noise"
    print " 11 --> Measured Laser Wavelength"
    print " 12 --> Resampling laser Wavelength"
    print " 13 --> Monitored laser Wavelength"
    print " 14 --> Ds symmetry"
    print " 15 --> Ds spectra Stability"
    print " 16 --> Ict Spectra stability"
    print " 17 --> Ict Temperature stability"
    print " 18 --> Ict Temperature consistency"
    print " 19 --> Number of valid PRT temps"
    
    input_var = raw_input("Enter the number from above list  :  ")
    
    if input_var == '1':
        import plot_qf1
        istring = 'All_Data/CrIS-SDR_All/QF1_SCAN_CRISSDR'
        data = read_cris_data_files_v2.read_h5_files(istring, pathToSdr, pathToSdr)
        plot_qf1.qf1_flag(data, pathToSdr)
        
        
    elif input_var == '2':
        import plot_qf2_v2
        istring = 'All_Data/CrIS-SDR_All/QF2_CRISSDR'
        data = read_cris_data_files_v2.read_h5_files(istring, pathToSdr, pathToSdr)
        plot_qf2_v2.qf2_flag(data, pathToSdr)
        
    elif input_var == '3':
        import plot_qf3_v2
        istring = 'All_Data/CrIS-SDR_All/QF3_CRISSDR'
        data = read_cris_data_files_v2.read_h5_files(istring, pathToSdr, pathToSdr)
        plot_qf3_v2.qf3_flag(data, pathToSdr )
        
    elif input_var == '4':
        import plot_qf4
        istring = 'All_Data/CrIS-SDR_All/QF4_CRISSDR'
        data = read_cris_data_files_v2.read_h5_files(istring, pathToSdr, pathToSdr)
        plot_qf4.qf4_flag(data)
        
    elif input_var == '5':
        import plot_ds_window_size_v2
        istring = 'All_Data/CrIS-SDR_All/DS_WindowSize'
        data = read_cris_data_files_v2.read_h5_files(istring, pathToSdr, pathToSdr)
        plot_ds_window_size_v2.ds_window_size(data)
        
    elif input_var == '6':
        import plot_ict_window_size_v2
        istring = 'All_Data/CrIS-SDR_All/ICT_WindowSize'
        data = read_cris_data_files_v2.read_h5_files(istring, pathToSdr, pathToSdr)
        plot_ict_window_size_v2.ict_window_size(data)
        
    elif input_var == '7':
        import plot_ES_ZPD_amplitude_v2
        istring = 'All_Data/CrIS-SDR_All/ES_ZPDAmplitude'
        data = read_cris_data_files_v2.read_h5_files(istring, pathToSdr, pathToSdr)
        plot_ES_ZPD_amplitude_v2.es_zpd_amplitude(data)
        
    elif input_var == '8':
        import plot_ES_ZPD_fringecount_v2
        istring = 'All_Data/CrIS-SDR_All/ES_ZPDFringeCount'
        data = read_cris_data_files_v2.read_h5_files(istring, pathToSdr, pathToSdr)
        plot_ES_ZPD_fringecount_v2.es_zpd_fringecount(data)
        
    elif input_var == '9':
        import plot_SDR_fringe_count_v2
        istring = 'All_Data/CrIS-SDR_All/SDRFringeCount'
        data = read_cris_data_files_v2.read_h5_files(istring, pathToSdr, pathToSdr)
        plot_SDR_fringe_count_v2.sdr_fringecount(data)
        
    elif input_var == '10':
        import plot_es_RDR_impulse_v2
        istring = 'All_Data/CrIS-SDR_All/ES_RDRImpulseNoise'
        data = read_cris_data_files_v2.read_h5_files(istring, pathToSdr, pathToSdr)
        plot_es_RDR_impulse_v2.es_rdr_impulse(data)
        
    elif input_var == '11':
        import plot_measured_laser_wavelen
        istring = 'All_Data/CrIS-SDR_All/MeasuredLaserWavelength'
        data = read_cris_data_files_v2.read_h5_files(istring, pathToSdr, pathToSdr)
        plot_measured_laser_wavelen.measured_laser_wavelen(data)
        
    elif input_var == '12':
        import plot_resampling_laser_wavelen
        istring = 'All_Data/CrIS-SDR_All/ResamplingLaserWavelength'
        data = read_cris_data_files_v2.read_h5_files(istring, pathToSdr, pathToSdr)
        plot_resampling_laser_wavelen.resampling_laser_wavelen(data)
        
    elif input_var == '13':
        import plot_monitored_laser_wavelen
        istring = 'All_Data/CrIS-SDR_All/MonitoredLaserWavelength'
        data = read_cris_data_files_v2.read_h5_files(istring, pathToSdr, pathToSdr)
        plot_monitored_laser_wavelen.monitored_laser_wavelen(data)
    
    elif input_var == '14':
        import plot_ds_symmetry_v2
        istring = 'All_Data/CrIS-SDR_All/DS_Symmetry'
        data = read_cris_data_files_v2.read_h5_files(istring, pathToSdr, pathToSdr)
        plot_ds_symmetry_v2.ds_symmetry(data)
        
    elif input_var == '15':
        import plot_ds_spectra_stability_v2
        istring = 'All_Data/CrIS-SDR_All/DS_SpectralStability'
        data = read_cris_data_files_v2.read_h5_files(istring, pathToSdr, pathToSdr)
        plot_ds_spectra_stability_v2.ds_spectra_stability(data)
     
    elif input_var == '16':
        import plot_ict_spectra_stability_v2
        istring = 'All_Data/CrIS-SDR_All/ICT_SpectralStability'
        data = read_cris_data_files_v2.read_h5_files(istring, pathToSdr, pathToSdr)
        plot_ict_spectra_stability_v2.ict_spectra_stability(data)
        
    elif input_var == '17':
        import plot_ict_temp_stability_v2
        istring = 'All_Data/CrIS-SDR_All/ICT_TemperatureStability'
        data = read_cris_data_files_v2.read_h5_files(istring, pathToSdr, pathToSdr)
        plot_ict_temp_stability_v2.ict_temp_stability(data)
    
    elif input_var == '18':
        import plot_ict_temp_consistency
        istring = 'All_Data/CrIS-SDR_All/ICT_TemperatureConsistency'
        data = read_cris_data_files_v2.read_h5_files(istring, pathToSdr, pathToSdr)
        plot_ict_temp_consistency.ict_temp_consistency(data)
        
    elif input_var == '19':
        import plot_num_valid_prt_temp
        istring = 'All_Data/CrIS-SDR_All/NumberOfValidPRTTemps'
        data = read_cris_data_files_v2.read_h5_files(istring, pathToSdr, pathToSdr)
        plot_num_valid_prt_temp.valid_prt_temp(data)
        
    elif input_var == '20':
        import plot_CRIS_SDR_spectra
        plot_CRIS_SDR_spectra.cris_sdr_spectra(pathToSdr, pathToSdr)
      
        
    else :
        print "Wrong input, Please try again"
        
        
     #plot_CRIS_SDR_BT.cris_sdr_brightnesstemp(sdrLwSpectra, sdrMwSpectra, sdrSwSpectra)
     
    print "hey"
    
    #arrange_cris_data.cris_data_rearrange(data)
    print "total time of execution", (time.time() - start_time)

if __name__ == '__main__':
    main()