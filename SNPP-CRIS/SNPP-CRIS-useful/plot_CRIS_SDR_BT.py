#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import plancks_fxn


def cris_sdr_brightnesstemp(dataLw, dataMw, dataSw):

  
    dataArrayLw = np.array(dataLw)
    dataArrayMw = np.array(dataMw)
    dataArraySw = np.array(dataSw)
    
    # frequency scale
    #
    lwfreq = np.arange(648.75, 1096.875, 0.625)
    mwfreq = np.arange(1208.75, 1751.875, 0.625)
    swfreq = np.arange(2153.75, 2551.875, 0.625)
  
    crisBTLwFov = dict()
    crisBTMWFov = dict()
    crisBTSWFov = dict()
        
    for i in range(9):
        crisBTLwFov[i] = plancks_fxn.planck_bth(dataArrayLw[18,0,14,i,:], lwfreq)
        crisBTMWFov[i] = plancks_fxn.planck_bth(dataArrayMw[18,0,14,i,:], mwfreq)
        crisBTSWFov[i] = plancks_fxn.planck_bth(dataArraySw[18,0,14,i,:], swfreq)
     
    color = ['orangered','r','c','m','y','lime','k','g','b']
    
    fig, ax= plt.subplots(nrows=1,ncols=1, sharex=True, sharey=True, squeeze=True)
   
    fig.subplots_adjust(bottom=0.24)
    
    p = dict()
    for j in range(9):  
        
       p[j], = ax.plot(lwfreq, crisBTLwFov[j], color = color[j] , markersize = 7, clip_on=False) 
       ax.plot(mwfreq, crisBTMWFov[j], color = color[j], markersize = 7, clip_on=False) 
       ax.plot(swfreq, crisBTSWFov[j], color = color[j], markersize = 7, clip_on=False) 
       
             
    fig.legend( (p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7], p[8]), ('fov1', 'fov2','fov3','fov4','fov5','fov6', 'fov7','fov8','fov9'), loc = 'lower center',  ncol=4, labelspacing=0.0, prop={'size':9})  
    
    ax.set_title('CrIS Spectral Radiance', fontsize =8)
    
    ax.set_xlabel('Wavenumber (cm-1)')
    ax.set_ylabel('Kelvin (K)')
    
    ax.set_xlim(600,3000)
    ax.set_ylim(140,260)
   
    plt.grid(True)
    plt.suptitle('CrIS hyperspectral Brightness Temp for LW, MW, SW')
    
    print " quality flag3 plotted"
