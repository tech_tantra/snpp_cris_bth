#!/usr/bin/env python

# 
import matplotlib as mpl
mpl.use('TkAgg')
#mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.ticker import FixedLocator
import numpy as np
import plancks_fxn
import time

def cris_world_plot(m1, numAs, numDs, forLatAs, forLonAs, forSpectraAs, forLatDs, forLonDs, forSpectraDs, nameStr):
    
    start_time =time.time()    
    
    earthScenes =30
    fov = 9
        
    if nameStr == '900cm-1':
        titlenameAs = 'CrIS FSR BT, 11 um (900 cm-1), Ascending'
        titlenameDs = 'CrIS FSR BT, 11 um (900 cm-1), Descending'
        vmx = 310
        vmn = 210
        wn = 900.0
    elif nameStr == 'img900cm-1':
        titlenameAs = 'CrIS FSR Img rad, 11 um (900 cm-1), Ascending'
        titlenameDs = 'CrIS FSR Img rad, 11 um (900 cm-1), Descending'
        vmx = 0.5
        #vmx = 310
        vmn = -0.5
        #vmn =210
        wn = 900.0
    elif nameStr =='1500cm-1':
        titlenameAs = 'CrIS FSR BT , 6.7 um (1500 cm-1), Ascending'
        titlenameDs = 'CrIS FSR BT , 6.7 um (1500 cm-1), Descending'
        vmx = 275
        vmn = 225
        wn = 1500.0
    elif nameStr =='img1500cm-1':
        titlenameAs = 'CrIS FSR Img rad, 11 um (1500 cm-1), Ascending'
        titlenameDs = 'CrIS FSR Img rad, 11 um (1500 cm-1), Descending'
        vmx = 0.5
        vmn = -0.5
        wn = 1500.0
    elif nameStr =='2500cm-1':
        titlenameAs = 'CrIS FSR BT, 11 um (2500 cm-1), Ascending'
        titlenameDs = 'CrIS FSR BT, 11 um (2500 cm-1), Descending'
        vmx = 310
        vmn = 210   
        wn = 2500.0
    elif nameStr =='img2500cm-1':
        titlenameAs = 'CrIS FSR Img rad, 11 um (2500 cm-1), Ascending'
        titlenameDs = 'CrIS FSR Img rad, 11 um (2500 cm-1), Descending'
        vmx = 0.02
        vmn = -0.02
        wn = 2500.0
    elif nameStr =='sdrQfLw':
        titlenameAs = 'CrIS FSR SDR LW Overall QF, Ascending'
        titlenameDs = 'CrIS FSR SDR LW Overall QF, Desending'
        mask_value = 0b00000011
        vmn = 0
        vmx = 3
        #colors = ['b','g','r','k']
        colors = ['dodgerblue', 'lime', 'red', 'black']
        
    elif nameStr == 'sdrQfMw':
        titlenameAs = 'CrIS FSR SDR MW Overall QF, Ascending'
        titlenameDs = 'CrIS FSR SDR MW Overall QF, Desending'
        mask_value = 0b00000011
        #colors = ['b','g','r','k']
        colors = ['dodgerblue', 'lime', 'red', 'black']
        vmn = 0
        vmx = 3
    elif nameStr == 'sdrQfSw':
        titlenameAs = 'CrIS FSR SDR SW Overall QF, Ascending'
        titlenameDs = 'CrIS FSR SDR SW Overall QF, Desending'  
        mask_value = 0b00000011
        vmn = 0
        vmx = 3
        #colors = ['b','g','r','k']
        colors = ['dodgerblue', 'lime', 'red', 'black']
        
    elif nameStr == 'rdrInvalidFlagLw':
        titlenameAs = 'CrIS FSR LW RDR Invalid Flag, Ascending'
        titlenameDs = 'CrIS FSR LW RDR Invalid Flag, Desending'  
        mask_value = 0b00000010
        vmn = 0
        vmx = 1
        #colors = ['b','r']
        colors = ['dodgerblue', 'red']
    elif nameStr == 'rdrInvalidFlagMw':
        titlenameAs = 'CrIS FSR MW RDR Invalid Flag, Ascending'
        titlenameDs = 'CrIS FSR MW RDR Invalid Flag, Desending'  
        mask_value = 0b00000010
        vmn = 0
        vmx = 1
        #colors = ['b','r']
        colors = ['dodgerblue', 'red']
    elif nameStr == 'rdrInvalidFlagSw':
        titlenameAs = 'CrIS FSR SW RDR Invalid Flag, Ascending'
        titlenameDs = 'CrIS FSR SW RDR Invalid Flag, Desending'  
        mask_value = 0b00000010
        vmn = 0
        vmx = 1
        #colors = ['b','r']
        colors = ['dodgerblue', 'red']
    
    if numAs > 0:
        
        fig = plt.figure(figsize=(15, 10))
        ax1 = fig.add_subplot(211)
        ax1.xaxis.set_major_locator(FixedLocator(np.arange(-180, 181, 60)))
        ax1.yaxis.set_major_locator(FixedLocator(np.arange(-90, 91, 30)))
        ax1.set_xticklabels(['180W', '120W', '60W', '0', '60E', '120E', '180E'])
        ax1.set_yticklabels(['90S', '60S', '30S', 'EQ', '30N', '60N', '90N'])
        ax1.set_title(titlenameAs )
        m1.drawcoastlines(linewidth=0.7, color='grey')
        
        if nameStr == 'img900cm-1' or nameStr == 'img1500cm-1' or nameStr == 'img2500cm-1':
            for bt in range(fov):
                #fovLwBtAs =   planck_bth(np.concatenate(forSpectraAs[bt]), waven)
                fovLwRadAs = np.concatenate(forSpectraAs[bt])
                xd, yd = m1(np.concatenate(forLonAs[bt]), np.concatenate(forLatAs[bt]))
                img1 = m1.scatter( xd, yd, s=1.0, c=fovLwRadAs, vmin =vmn, vmax =vmx, edgecolor='none', cmap='jet' ) #cmap = 'rainbow' or 'jet'
                #del fovLwRadAs
        
        elif nameStr == 'sdrQfLw' or nameStr=='sdrQfMw' or nameStr =='sdrQfSw':
           for bt in range(fov):
                #fovLwRadAs =   planck_bth(np.concatenate(forSpectraAs[bt]), waven)
                fovLwRadAs = np.concatenate(forSpectraAs[bt])
                fovLwQfAs = fovLwRadAs & mask_value
                cmap = mpl.colors.ListedColormap(colors)
                xd, yd = m1(np.concatenate(forLonAs[bt]), np.concatenate(forLatAs[bt]))
                img1 = plt.scatter( xd, yd, s=1.0, c=fovLwQfAs, vmin =vmn, vmax =vmx, edgecolor='none', cmap=cmap )
                #del fovLwRadAs
                
        elif nameStr == 'rdrInvalidFlagLw' or nameStr=='rdrInvalidFlagMw' or nameStr == 'rdrInvalidFlagSw':
           for bt in range(fov):
                #fovLwRadAs =   planck_bth(np.concatenate(forSpectraAs[bt]), waven)
                fovLwRadAs = np.concatenate(forSpectraAs[bt])
                fovLwQfAs = (fovLwRadAs & mask_value)/2
                cmap = mpl.colors.ListedColormap(colors)
                xd, yd = m1(np.concatenate(forLonAs[bt]), np.concatenate(forLatAs[bt]))
                img1 = plt.scatter( xd, yd, s=1.0, c=fovLwQfAs, vmin =vmn, vmax =vmx,  edgecolor='none', cmap=cmap )       
                 
        else:
        
            waven =  np.tile(wn, (len(forSpectraAs[1])*earthScenes))
    
            for bt in range(fov):
                fovLwBtAs =   plancks_fxn.planck_bth(np.concatenate(forSpectraAs[bt]), waven)
                xd, yd = m1(np.concatenate(forLonAs[bt]), np.concatenate(forLatAs[bt]))
                img1 = m1.scatter( xd, yd, s=1.0, c=fovLwBtAs, vmin =vmn, vmax =vmx,  edgecolor='none', cmap='jet' )
                #del fovLwBtAs
    
        
        plt.colorbar(img1, orientation= 'horizontal', ticks = [vmn, (vmn +(vmn+vmx)/2)/2, (vmn+vmx)/2, (((vmn+vmx)/2) + vmx)/2,  vmx ], shrink=0.5,fraction =0.07)
       
        del forSpectraAs
        del forLonAs
        del forLatAs
        
    else:
        print "There are no Ascending Granules"
    
    if numDs > 0:
        
        ax2 = fig.add_subplot(212)
        ax2.xaxis.set_major_locator(FixedLocator(np.arange(-180, 181, 60)))
        ax2.yaxis.set_major_locator(FixedLocator(np.arange(-90, 91, 30)))
        ax2.set_xticklabels(['180W', '120W', '60W', '0', '60E', '120E', '180E'])
        ax2.set_yticklabels(['90S', '60S', '30S', 'EQ', '30N', '60N', '90N'])
        ax2.set_title(titlenameDs )
        m1.drawcoastlines(linewidth=0.7, color='grey')
        
        if nameStr == 'img900cm-1' or nameStr == 'img1500cm-1' or nameStr == 'img2500cm-1':
            for bt in range(fov):
                #fovLwRadAs =   planck_bth(np.concatenate(forSpectraAs[bt]), waven)
                fovLwRadDs = np.concatenate(forSpectraDs[bt])
                xd, yd = m1(np.concatenate(forLonDs[bt]), np.concatenate(forLatDs[bt]))
                img2 = plt.scatter( xd, yd, s=1.0, c=fovLwRadDs, vmin =vmn, vmax =vmx,  edgecolor='none', cmap='jet' )
                del fovLwRadDs
                
        elif nameStr == 'sdrQfLw' or nameStr=='sdrQfMw' or nameStr == 'sdrQfSw':
           for bt in range(fov):
                #fovLwRadAs =   planck_bth(np.concatenate(forSpectraAs[bt]), waven)
                fovLwRadDs = np.concatenate(forSpectraDs[bt])
                fovLwQfDs = fovLwRadDs & mask_value
                cmap = mpl.colors.ListedColormap(colors)
                xd, yd = m1(np.concatenate(forLonDs[bt]), np.concatenate(forLatDs[bt]))
                img2 = plt.scatter( xd, yd, s=1.0, c=fovLwQfDs, vmin =vmn, vmax =vmx,  edgecolor='none', cmap=cmap )
                #del fovLwRadDs
            
        elif nameStr == 'rdrInvalidFlagLw' or nameStr=='rdrInvalidFlagMw' or nameStr == 'rdrInvalidFlagSw':
           for bt in range(fov):
                #fovLwRadAs =   planck_bth(np.concatenate(forSpectraAs[bt]), waven)
                fovLwRadDs = np.concatenate(forSpectraDs[bt])
                fovLwQfDs = (fovLwRadDs & mask_value)/2
                cmap = mpl.colors.ListedColormap(colors)
                xd, yd = m1(np.concatenate(forLonDs[bt]), np.concatenate(forLatDs[bt]))
                img2 = plt.scatter( xd, yd, s=1.0, c=fovLwQfDs, vmin =vmn, vmax =vmx,  edgecolor='none', cmap=cmap )     
            
            
        else:   
            waven1 =  np.tile(wn, (len(forSpectraDs[1])*earthScenes))
            for dt in range(fov):
                fovLwBtDs =   plancks_fxn.planck_bth(np.concatenate(forSpectraDs[dt]), waven1)
                xd, yd = m1(np.concatenate(forLonDs[dt]), np.concatenate(forLatDs[dt]))
                img2 = plt.scatter( xd, yd, s=1.0, c=fovLwBtDs, vmin =vmn, vmax =vmx, edgecolor='none', cmap='jet' )
                del fovLwBtDs
    
    
        del forSpectraDs
        del forLonDs
        del forLatDs
        
        plt.colorbar(img2, orientation= 'horizontal',ticks = [vmn, (vmn +(vmn+vmx)/2)/2, (vmn+vmx)/2, ((vmn+vmx)/2 + vmx)/2,  vmx ], shrink=0.5, fraction =0.07)# fraction=0.07, pad=0.04 )
        
        #fname = 'C:\\Users\\suman\Desktop\\python_codes\\plots\\' + nameStr + '.jpg'
        fname = 'C:\\Users\\sbhatta\Desktop\\w_plots\\' + nameStr+'.jpeg'
        #fig.canvas.draw()
        #Supported formats: eps, jpeg, jpg, pdf, pgf, png, ps, raw, rgba, svg, svgz, tif, tiff.
        plt.savefig(fname, dpi=100, bbox_inches='tight')
        #plt.close()
        #plt.show()
        print "Total time to plot", time.time() - start_time
        return 0    