#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec

import time
import h5py
import glob
import itertools


def read_h5_files_spectra(geoLoc, radLoc):
    geofileName = sorted(glob.glob(geoLoc+'\GCRSO_npp*'))
    radfileName = sorted(glob.glob(radLoc+'\SCRIS_npp*'))
    
    stTime = []
    #obsTime = []
    dataLw =[]
    dataMw =[]
    dataSw =[]
    lat =[]
    lon =[]
    
    for geoFile, radFile in itertools.izip(geofileName, radfileName ):
        geo =  h5py.File(geoFile, 'r', driver='core')
        #lat.append(geo['All_Data']['CrIS-SDR-GEO_All']['Latitude'][:])
        #lon.append(geo['All_Data']['CrIS-SDR-GEO_All']['Longitude'][:])
        stTime.append(geo['All_Data']['CrIS-SDR-GEO_All']['StartTime'][:])
        lat = geo['All_Data']['CrIS-SDR-GEO_All']['Latitude'][:]
        lon = geo['All_Data']['CrIS-SDR-GEO_All']['Longitude'][:]

        rad = h5py.File(radFile, 'r', driver='core')
        #obsTime.append(rad['Data_Products']['CrIS-SDR']['CrIS-SDR_Gran_0'].attrs['Beginning_Time'])
        dataLw.append(rad['All_Data']['CrIS-SDR_All']['ES_RealLW'][:])
        dataMw.append(rad['All_Data']['CrIS-SDR_All']['ES_RealMW'][:])
        dataSw.append(rad['All_Data']['CrIS-SDR_All']['ES_RealSW'][:])
    geo.close()
    rad.close()
    data={'dataLw':dataLw, 'dataMw':dataMw, 'dataSw':dataSw }
    
    return (data)



def display_spectra(dataArrayLw, dataArrayMw, dataArraySw, input_var  ):   
    crisSDRSpectraLWFov = dict()
    crisSDRSpectraMWFov = dict()
    crisSDRSpectraSWFov = dict()
        
    gx = [0, 0, 0, 1, 1, 1, 2, 2, 2]
    gy = [0, 1, 2, 0, 1, 2, 0, 1, 2]
    color = ['orangered','r','c','m','y','lime','k','g','b']
    gs = gridspec.GridSpec(3,3)    
    fig = plt.figure(figsize = (15,8))   
    
    # frequency scale
    #
    lwfreq = np.arange(648.75, 1096.875, 0.625)
    mwfreq = np.arange(1208.75, 1751.875, 0.625)
    swfreq = np.arange(2153.75, 2551.875, 0.625)
    
        
    for f in range(9):
        crisSDRSpectraLWFov[f] = dataArrayLw[0,0,input_var,f,:]
        crisSDRSpectraMWFov[f] = dataArrayMw[0,0,input_var,f,:]
        crisSDRSpectraSWFov[f] = dataArraySw[0,0,input_var,f,:]
                        
                            
        #ploting
                
        ax1 = plt.subplot(gs[gx[f], gy[f]])
        ax1.plot(lwfreq, crisSDRSpectraLWFov[f], color = color[f] , markersize = 7, clip_on=False)
        ax1.plot(mwfreq, crisSDRSpectraMWFov[f], color = color[f], markersize = 7, clip_on=False)
        ax1.plot(swfreq, crisSDRSpectraSWFov[f], color = color[f], markersize = 7, clip_on=False)
        ax1.set_title("Radiance Spec(LW,MW,SW)" + " FOV "+ str(f+1) , fontsize =8)
        ax1.set_ylabel('Radiance', fontsize =8)
        ax1.set_xlabel('wavenumber (cm-1)', fontsize =8)
        ax1.grid(True)
            #ax1.set_grid(True)
        ax1.set_xlim(600,2600)
        ax1.set_ylim(-10,60)
            #ax1.set_yticks(major_yticks)
     
        #plt.grid(True)
        plt.suptitle('CrIS hyperspectral Radiance data for LW, MW, SW', y=1.05)
        plt.tight_layout(pad =0.2, w_pad=0.5, h_pad=0.50)
        
    #exit(0)
    
    


def cris_sdr_spectra(radLoc, geoLoc):
    
    data = read_h5_files_spectra(radLoc, geoLoc)
#    qf2b1 = [qf[0][:][:][0] for qf in data]
#    qf2b2 = [qf[0][:][:][1] for qf in data]
#    qf2b3 = [qf[0][:][:][2] for qf in data]
#    
    dataLw = data['dataLw']
    dataMw = data['dataMw']
    dataSw = data['dataSw']
    
    dataArrayLw = np.array(dataLw)
    dataArrayMw = np.array(dataMw)
    dataArraySw = np.array(dataSw)
    
    print "shape of the ds spectra:", np.shape(dataArraySw)
    
    #crisFreq = np.arange(648.75, 2551.875, 0.625)
    
    input_var = raw_input("Enter the required FOR (0-31) to plot :  ")
    
    
    if (int(input_var) >=0 and int(input_var) <=31):
        display_spectra(dataArrayLw, dataArrayMw, dataArraySw, input_var )
         #exit(0)
            
    else:
        print " wrong input, Try again: "
        cris_sdr_spectra(radLoc, geoLoc)        
        
       
