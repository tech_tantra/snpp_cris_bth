#!/usr/bin/env python
# 
# get spectra from sdrs
#
import time
import h5py
import glob
import itertools


def read_h5_files(inputString, radLoc, geoLoc):
    geofileName = sorted(glob.glob(geoLoc+'\GCRSO_npp*'))
    radfileName = sorted(glob.glob(radLoc+'\SCRIS_npp*'))
    
    stTime = []
    #obsTime = []
    data = []
    
    if len(inputString) >1:
        for geoFile, radFile in itertools.izip(geofileName, radfileName ):
            geo =  h5py.File(geoFile, 'r', driver='core')
        #lat.append(geo['All_Data']['CrIS-SDR-GEO_All']['Latitude'][:])
        #lon.append(geo['All_Data']['CrIS-SDR-GEO_All']['Longitude'][:])
            stTime.append(geo['All_Data']['CrIS-SDR-GEO_All']['StartTime'][:])

            rad = h5py.File(radFile, 'r', driver='core')
        #obsTime.append(rad['Data_Products']['CrIS-SDR']['CrIS-SDR_Gran_0'].attrs['Beginning_Time'])
            data.append(rad[inputString][:])
        geo.close()
        rad.close()
        
    return data
        
    
    
    for geoFile, radFile in itertools.izip(geofileName, radfileName ):
        geo =  h5py.File(geoFile, 'r', driver='core')
        #lat.append(geo['All_Data']['CrIS-SDR-GEO_All']['Latitude'][:])
        #lon.append(geo['All_Data']['CrIS-SDR-GEO_All']['Longitude'][:])
        stTime.append(geo['All_Data']['CrIS-SDR-GEO_All']['StartTime'][:])

        rad = h5py.File(radFile, 'r', driver='core')
        #obsTime.append(rad['Data_Products']['CrIS-SDR']['CrIS-SDR_Gran_0'].attrs['Beginning_Time'])
        data.append(rad[inputString][:])
    geo.close()
    rad.close()
        
    return data
 
    
